from abc import ABC

class Animal(ABC):
    def eat(self, food):
        pass
    def make_sound():
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    
    #getter
    def get_name(self):
        print(f"Cat name is : {self._name}")
    def get_breed(self):
        print(f"Cat breed is : {self._breed}")
    def get_age(self):
        print(f"Cat age is: {self._age}")

    #setter
    def set_name(self, name):
        self._name = name
    def set_breed(self, breed):
        self._breed = breed
    def set_age(self, age):
        self._age = age 
    
    def eat(self, food):
        print(f"Serve me {food}")

    def call(self):
        print(f"{self._name} come on!")

    def make_sound(self):
        print("Miaw! Nyaw! Nyaaaaa!")


class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    #getter
    def get_name(self):
        print(f"Dog name is : {self._name}")
    def get_breed(self):
        print(f"Dog breed is : {self._breed}")
    def get_age(self):
        print(f"Dog age is: {self._age}")

    #setter
    def set_name(self, name):
        self._name = name
    def set_breed(self, breed):
        self._breed = breed
    def set_age(self, age):
        self._age = age    
    
    def eat(self, food):
        print(f"Eaten {food}")

    def call(self):
        print(f"Here {self._name}!")

    def make_sound(self):
        print("Bark! Woof! Arf!")

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
